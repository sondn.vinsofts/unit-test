<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Stupid;

class StupidTest extends TestCase
{
    public function testTrueIsTrue()
    {
        // arrange
        $expectation = true;

        // act
        $stupid = new Stupid();
        $result = $stupid->returnTrue();

        // assert
        $this->assertEquals($result, $expectation);
    }
}
