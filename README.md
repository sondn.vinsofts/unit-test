### 1. Cấu trúc thư mục
- Gồm 2 bài tập, chia thành 2 folder `Exercise 1` và `Exercise 2`
- Mỗi folder exercise có template riêng cho ngôn ngữ PHP và Typescript để phù hợp với từng người
- Đề bài nằm trong file README ở folder tương ứng

### 2. Yêu cầu
- Chương trình chạy không có error/exception

### 2. Submit bài làm
- Tạo merge request vào branch master của repo này