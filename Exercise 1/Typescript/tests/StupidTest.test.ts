import { returnTrue } from '../src/Stupid';

test('Test true is true', async () => {
    // arrange
    const expectation = true;

    // act
    const result = returnTrue();

    // assert
    expect(result).toBe(expectation);
});