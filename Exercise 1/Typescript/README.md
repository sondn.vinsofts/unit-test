### Bước 1. Cài đặt thư viện bằng
```
$ npm install
```

### Bước 2. Viết test và code trong ./tests và ./src

### Bước 3. Chạy unit test bằng
```
$ npm test
```