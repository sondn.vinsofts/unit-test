### Đề bài:

Mô phỏng việc chào hỏi.

User truyền một câu chào dưới dạng param vào function ( ví dụ: greeting() )
- Nếu param là "Hi" hoặc "Hello": tùy theo thời điểm trong ngày mà giá trị trả về là:
    - Từ 6h đến 12h: "Good morning"
    - Từ 12h đến 18h: "Good afternoon"
    - Từ 18h đến 6h: "Good evening"
- Nếu param không phải "Hi" hoặc "Hello": trả về  "That's not how you greet"